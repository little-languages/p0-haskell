{
module P0.Lexical
  ( Lexer
  , Token(..)
  , evalLexer
  , lexeme
  , lexer
  , readToken
  , scanner
  ) where

import           Data.Char (ord)
import           Data.Text (Text)
import qualified Data.Text as Text
import           Data.Word (Word8)
import           Control.Monad.Except (throwError)
import           Control.Monad.State (StateT, evalStateT, get, put)

import           P0.Errors (Error(..))
import           P0.Position (Point(..), Position (..), Positionable(..), mkPosition, positionLength, startPoint)
import qualified P0.StringConversion as SC
}

$digit = 0-9
$alpha = [a-zA-Z]
$alphaDigit = [0-9a-zA-Z]

tokens :-
  $white+                               ;
  "//".*                                { mkToken TSingleLine }
  "/*"                                  { matchMultilineComments }

  "const"                               { mkToken' TConst }
  "else"                                { mkToken' TElse }
  "false"                               { mkToken' TFalse }
  "fun"                                 { mkToken' TFun }
  "if"                                  { mkToken' TIf }
  "let"                                 { mkToken' TLet }
  "return"                              { mkToken' TReturn }
  "true"                                { mkToken' TTrue }
  "while"                               { mkToken' TWhile }

  "Bool"                                { mkToken' TBool }
  "Float"                               { mkToken' TFloat }
  "Int"                                 { mkToken' TInt }

  $digit+                               { mkToken TLiteralInt }
  $digit+\.$digit+ (("e" | "E") ("+" | "-")? $digit+)? |
    \.$digit+ (("e" | "E") ("+" | "-")? $digit+)? |
    $digit+ (("e" | "E") ("+" | "-")? $digit+)?
                                        { mkToken TLiteralFloat }
  \" (~ [\\\"] | \\ \" | \\ \\) * \"    { mkToken TLiteralString }
  $alpha $alphaDigit*                   { mkToken TIdentifier }

  \&\&                                  { mkToken' TAmpersandAmpersand }
  \!                                    { mkToken' TBang }
  \!\=                                  { mkToken' TBangEqual }
  \|\|                                  { mkToken' TBarBar }
  \:                                    { mkToken' TColon }
  \,                                    { mkToken' TComma }
  \=                                    { mkToken' TEqual }
  \=\=                                  { mkToken' TEqualEqual }
  \>\=                                  { mkToken' TGreaterEqual }
  \>                                    { mkToken' TGreaterThan }
  \{                                    { mkToken' TLCurly }
  \<\=                                  { mkToken' TLessEqual }
  \<                                    { mkToken' TLessThan }
  \(                                    { mkToken' TLParen }
  \-                                    { mkToken' TMinus }
  \+                                    { mkToken' TPlus }
  \?                                    { mkToken' TQuestion }
  \}                                    { mkToken' TRCurly }
  \)                                    { mkToken' TRParen }
  \;                                    { mkToken' TSemicolon }
  \/                                    { mkToken' TSlash }
  \*                                    { mkToken' TStar }

{

data Token
  = TConst Position
  | TElse Position
  | TFalse Position
  | TFun Position
  | TIf Position
  | TLet Position
  | TReturn Position
  | TTrue Position
  | TWhile Position

  | TBool Position
  | TFloat Position
  | TInt Position

  | TIdentifier Position Text
  | TLiteralInt Position Text
  | TLiteralFloat Position Text
  | TLiteralString Position Text

  | TAmpersandAmpersand Position
  | TBang Position
  | TBangEqual Position
  | TBarBar Position
  | TColon Position
  | TComma Position
  | TEqual Position
  | TEqualEqual Position
  | TGreaterEqual Position
  | TGreaterThan Position
  | TLCurly Position
  | TLessEqual Position
  | TLessThan Position
  | TLParen Position
  | TMinus Position
  | TPlus Position
  | TQuestion Position
  | TRCurly Position
  | TRParen Position
  | TSemicolon Position
  | TSlash Position
  | TStar Position

  | TEOS Position
  | TSingleLine Position Text
  | TMultiLine Position Text
  deriving (Eq)


instance Show Token where
  show (TEOS p) = "EOS " ++ show p ++ " []"
  show t = (name t) ++ " " ++ show (position t) ++ " [" ++ (SC.textToString $ lexeme t) ++ "]"


instance Positionable Token where
  position (TConst p) = p
  position (TElse p) = p
  position (TFalse p) = p
  position (TFun p) = p
  position (TIf p) = p
  position (TLet p) = p
  position (TReturn p) = p
  position (TTrue p) = p
  position (TWhile p) = p
  position (TBool p) = p
  position (TFloat p) = p
  position (TInt p) = p

  position (TIdentifier p _) = p
  position (TLiteralInt p _) = p
  position (TLiteralFloat p _) = p
  position (TLiteralString p _) = p

  position (TAmpersandAmpersand p) = p
  position (TBang p) = p
  position (TBangEqual p) = p
  position (TBarBar p) = p
  position (TColon p) = p
  position (TComma p) = p
  position (TEqual p) = p
  position (TEqualEqual p) = p
  position (TGreaterEqual p) = p
  position (TGreaterThan p) = p
  position (TLCurly p) = p
  position (TLessEqual p) = p
  position (TLessThan p) = p
  position (TLParen p) = p
  position (TMinus p) = p
  position (TPlus p) = p
  position (TQuestion p) = p
  position (TRCurly p) = p
  position (TRParen p) = p
  position (TSemicolon p) = p
  position (TSlash p) = p
  position (TStar p) = p
  
  position (TEOS p) = p
  position (TSingleLine p _) = p
  position (TMultiLine p _) = p


lexeme (TConst _) = "const"
lexeme (TElse _) = "else"
lexeme (TFalse _) = "false"
lexeme (TFun _) = "fun"
lexeme (TIf _) = "if"
lexeme (TLet _) = "let"
lexeme (TReturn _) = "return"
lexeme (TTrue _) = "true"
lexeme (TWhile _) = "while"
lexeme (TBool _) = "Bool"
lexeme (TFloat _) = "Float"
lexeme (TInt _) = "Int"
lexeme (TIdentifier _ l) = l
lexeme (TLiteralInt _ l) = l
lexeme (TLiteralFloat _ l) = l
lexeme (TLiteralString _ l) = l
lexeme (TAmpersandAmpersand _) = "&&"
lexeme (TBang _) = "!"
lexeme (TBangEqual _) = "!="
lexeme (TBarBar _) = "||"
lexeme (TColon _) = ":"
lexeme (TComma _) = ","
lexeme (TEqual _) = "="
lexeme (TEqualEqual _) = "=="
lexeme (TGreaterEqual _) = ">="
lexeme (TGreaterThan _) = ">"
lexeme (TLCurly _) = "{"
lexeme (TLessEqual _) = "<="
lexeme (TLessThan _) = "<"
lexeme (TLParen _) = "("
lexeme (TMinus _) = "-"
lexeme (TPlus _) = "+"
lexeme (TQuestion _) = "?"
lexeme (TRCurly _) = "}"
lexeme (TRParen _) = ")"
lexeme (TSemicolon _) = ";"
lexeme (TSlash _) = "/"
lexeme (TStar _) = "*"
lexeme (TEOS _) = ""
lexeme (TSingleLine _ l) = l
lexeme (TMultiLine _ l) = l

name (TConst _) = "Const"
name (TElse _) = "Else"
name (TFalse _) = "False"
name (TFun _) = "Fun"
name (TIf _) = "If"
name (TLet _) = "Let"
name (TReturn _) = "Return"
name (TTrue _) = "True"
name (TWhile _) = "While"
name (TBool _) = "Bool"
name (TFloat _) = "Float"
name (TInt _) = "Int"
name (TIdentifier _ _) = "Identifier"
name (TLiteralInt _ _) = "LiteralInt"
name (TLiteralFloat _ _) = "LiteralFloat"
name (TLiteralString _ _) = "LiteralString"
name (TAmpersandAmpersand _) = "AmpersandAmpersand"
name (TBang _) = "Bang"
name (TBangEqual _) = "BangEqual"
name (TBarBar _) = "BarBar"
name (TColon _) = "Colon"
name (TComma _) = "Comma"
name (TEqual _) = "Equal"
name (TEqualEqual _) = "EqualEqual"
name (TGreaterEqual _) = "GreaterEqual"
name (TGreaterThan _) = "GreaterThan"
name (TLCurly _) = "LCurly"
name (TLessEqual _) = "LessEqual"
name (TLessThan _) = "LessThan"
name (TLParen _) = "LParen"
name (TMinus _) = "Minus"
name (TPlus _) = "Plus"
name (TQuestion _) = "Question"
name (TRCurly _) = "RCurly"
name (TRParen _) = "RParen"
name (TSemicolon _) = "Semicolon"
name (TSlash _) = "Slash"
name (TStar _) = "Star"
name (TEOS _) = "EOS"
name (TSingleLine _ _) = "SingleLine"
name (TMultiLine _ _) = "MultiLine"


type AlexInput =
  ( Text        -- current input string
  , Point       -- current point position in the stream
  )


alexGetByte :: AlexInput -> Maybe (Word8, AlexInput)
alexGetByte (t, p) =
  if Text.null t
    then Nothing
    else let c = Text.head t
             s = Text.tail t
          in Just (toEnum $ ord c, (s, alexMove p c))
      where
        alexMove :: Point -> Char -> Point
        alexMove (Point offset line column) '\t' = Point (offset + 1) line       (column + alex_tab_size - ((column - 1) `mod` alex_tab_size))
        alexMove (Point offset line _) '\n'      = Point (offset + 1) (line + 1) 1
        alexMove (Point offset line column) _    = Point (offset + 1) line       (column + 1)


type Lexer a =
  StateT AlexInput (Either Error) a


evalLexer :: Lexer a -> AlexInput -> Either Error a
evalLexer = evalStateT


readToken :: Lexer Token
readToken = do
  preTokenInput <- get
  case alexScan preTokenInput 0 of
    AlexEOF ->
      return $ TEOS $ mkPosition (snd preTokenInput) 0
    AlexError (c, p) ->
      throwError $ LexicalError (mkPosition p 0) $ SC.stringToText $ show $ Text.take 1 c
    AlexSkip postTokenInput _ -> do
      put postTokenInput
      readToken
    AlexToken postTokenInput len tokenFunction ->
      tokenFunction preTokenInput postTokenInput len


mkToken :: (Position -> Text -> Token) -> AlexInput -> AlexInput -> Int -> Lexer Token
mkToken tk preTokenInput postTokenInput len = do
    put postTokenInput
    return $ tk (mkPosition (snd preTokenInput) len) $ Text.take len $ fst preTokenInput


mkToken' :: (Position -> Token) -> AlexInput -> AlexInput -> Int -> Lexer Token
mkToken' f = mkToken (\p _ -> f p)


matchMultilineComments :: AlexInput -> AlexInput -> Int -> Lexer Token
matchMultilineComments preTokenInput postTokenInput _ =
  go 1 postTokenInput
    where go :: Int -> AlexInput -> Lexer Token
          go 0 input = do
            put input
            let position = SourcePosition (snd preTokenInput) $ previous $ snd input
            return $ TMultiLine position $ Text.take (1 + (positionLength position)) $ fst preTokenInput
          go n input =
            case alexGetByte input of
              Just (42, input') -> -- '*'
                case alexGetByte input' of
                  Just (47, input'') -> -- '/'
                    go (n-1) input''
                  _ -> go n input'
              Just (47, input') -> -- '/'
                case alexGetByte input' of
                  Just (42, input'') -> -- '*'
                    go (n+1) input''
                  _ -> go n input'
              Just (_, input') -> go n input'
              Nothing -> throwError $ LexicalError (mkPosition (snd preTokenInput) 0) ""

          previous (Point offset line column) =
            Point (offset - 1) line (column - 1)

skipTokens :: Token -> Bool
skipTokens (TSingleLine _ _) = True
skipTokens (TMultiLine _ _ ) = True
skipTokens _ = False

lexer :: (Token -> Lexer a) -> Lexer a
lexer cont = do
  token <- readToken
  if skipTokens token
    then lexer cont
    else cont token

scanner :: Text.Text -> Either Error [Token]
scanner input = evalLexer readTokens (input, startPoint)
  where
    readTokens :: Lexer [Token]
    readTokens = do
      t <- readToken
      case t of
        TEOS _ -> return [t]
        _ -> do
          rest <- readTokens
          return (t : rest)
}
