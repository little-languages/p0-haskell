{
module P0.Static
  ( parse
  ) where

import           Control.Monad.Error (throwError)
import           Data.Text           (Text)

import           P0.Static.AST
import           P0.Errors           (Error(..))
import           P0.Lexical          (Lexer, Token(..), evalLexer, lexeme, lexer)
import           P0.Position         (Position (..), Positionable (..), combine, startPoint)
import qualified P0.StringConversion as SC
import qualified P0.Type             as Type
}

%name parseProgram
%monad { Lexer }
%lexer { lexer }{ TEOS _ }
%tokentype { Token }
%error { parseError }

%token
  Const                     { TConst _ }
  Else                      { TElse _ }
  False                     { TFalse _ }
  Fun                       { TFun _ }
  If                        { TIf _ }
  Let                       { TLet _ }
  Return                    { TReturn _ }
  True                      { TTrue _ }
  While                     { TWhile _ }

  Bool                      { TBool _ }
  Float                     { TFloat _ }
  Int                       { TInt _ }

  Identifier                { TIdentifier _ _ }
  LiteralInt                { TLiteralInt _ _ }
  LiteralFloat              { TLiteralFloat _ _ }
  LiteralString             { TLiteralString _ _ }

  AmpersandAmpersand        { TAmpersandAmpersand _ }
  Bang                      { TBang _ }
  BangEqual                 { TBangEqual _ }
  BarBar                    { TBarBar _ }
  Colon                     { TColon _ }
  Comma                     { TComma _ }
  Equal                     { TEqual _ }
  EqualEqual                { TEqualEqual _ }
  GreaterEqual              { TGreaterEqual _ }
  GreaterThan               { TGreaterThan _ }
  LCurly                    { TLCurly _ }
  LessEqual                 { TLessEqual _ }
  LessThan                  { TLessThan _ }
  LParen                    { TLParen _ }
  Minus                     { TMinus _ }
  Plus                      { TPlus _ }
  Question                  { TQuestion _ }
  RCurly                    { TRCurly _ }
  RParen                    { TRParen _ }
  Semicolon                 { TSemicolon _ }
  Slash                     { TSlash _ }
  Star                      { TStar _ }

%%

Program :: { Program }
  : Declarations                       { Program $1 }

Declarations :: { [Declaration] }
  : Declaration Declarations           { $1 : $2 }
  |                                    { [] }

Declaration :: { Declaration }
  : VariableDeclaration                { $1 }
  | FunctionDeclaration                { $1 }

VariableDeclaration :: { Declaration }
  : VariableDeclarationAccess Identifier Equal LiteralExpression Semicolon
                                       { VariableDeclaration $1 (mkIdentifier $2) $4 }

VariableDeclarationAccess :: { VariableAccess }
  : Const                              { ReadOnly }
  | Let                                { ReadWrite }

LiteralExpression :: { LiteralExpression }
  : True                               { LiteralExpressionValue $ LiteralBool (position $1) True }
  | False                              { LiteralExpressionValue $ LiteralBool (position $1) False }
  | LiteralExpressionSign LiteralExpressionValue
                                       { case $1 of
                                           Nothing -> LiteralExpressionValue $2
                                           Just (p, op) -> LiteralExpressionUnaryValue p op $2 }

LiteralExpressionSign :: { Maybe (Position, UnaryOp) }
  : Plus                               { Just (position $1, UnaryPlus) }
  | Minus                              { Just (position $1, UnaryMinus) }
  |                                    { Nothing }

LiteralExpressionValue :: { LiteralValue }
  : LiteralInt                         { LiteralInt (position $1) (lexeme $1) }
  | LiteralFloat                       { LiteralFloat (position $1) (lexeme $1) }

FunctionDeclaration :: { Declaration }
  : Fun Identifier LParen Parameters RParen FunctionDeclarationSuffix
                                       { FunctionDeclaration (mkIdentifier $2) $4 (fst $6) (snd $6) }

Parameters :: { [(Identifier, Type.Type)] }
  : TypedIdentifier ParametersSuffix   { $1 : $2 }
  |                                    { [] }

ParametersSuffix :: { [(Identifier, Type.Type)]}
  : Comma TypedIdentifier ParametersSuffix 
                                       { $2 : $3 }
  |                                    { [] }

FunctionDeclarationSuffix :: { ([Statement], Maybe (Type.Type, Expression)) }
  : Colon Type LCurly Statements Return Expression Semicolon RCurly
                                       { ($4, Just ($2, $6)) }
  | LCurly Statements RCurly           { ($2, Nothing) }

Statements :: { [Statement]}
  : Statement Statements               { $1 : $2 }
  |                                    { [] }

Type :: { Type.Type }
  : Int                                { Type.Int }
  | Float                              { Type.Float }
  | Bool                               { Type.Bool }

TypedIdentifier :: { (Identifier, Type.Type)}
  : Identifier Colon Type              { (mkIdentifier $1, $3) }

Statement :: { Statement }
  : Identifier Equal Expression Semicolon
                                       { AssignmentStatement Nothing (mkIdentifier $1) $3 }
  | AssignmentQualifier Identifier Equal Expression Semicolon
                                       { AssignmentStatement $1 (mkIdentifier $2) $4 }
  | If Expression Statement IfStatementSuffix
                                       { IfThenElseStatement $2 $3 $4 }
  | While Expression Statement         { WhileStatement $2 $3 }
  | LCurly Statements RCurly           { BlockStatement $2 }
  | Identifier LParen Arguments RParen Semicolon
                                       { CallStatement (mkIdentifier $1) $3 }
  | Semicolon                          { EmptyStatement }

AssignmentQualifier :: { Maybe VariableAccess }
  : Const                              { Just ReadOnly }
  | Let                                { Just ReadWrite }

IfStatementSuffix :: { Maybe Statement }
  : Else Statement                     { Just $2 }
  |                                    { Nothing }

Arguments :: { [Expression] }
  : Expression ArgumentsSuffix         { $1 : $2 }
  |                                    { [] }

ArgumentsSuffix :: { [Expression] }
  : Comma Expression ArgumentsSuffix   { $2 : $3 }
  |                                    { [] }

Expression :: { Expression }
  : OrExpression OptionalTernaryExpressionSuffix 
                                       { case $2 of
                                           Nothing -> $1
                                           Just (e2, e3) -> TernaryExpression $1 e2 e3 }

OptionalTernaryExpressionSuffix :: { Maybe (Expression, Expression ) }
  : Question Expression Colon Expression 
                                       { Just ($2, $4) }
  |                                    { Nothing }

OrExpression :: { Expression }
  : AndExpression OrExpressionSuffix   { $2 $1 }

OrExpressionSuffix :: { Expression -> Expression }
  : BarBar AndExpression OrExpressionSuffix 
                                       { \e -> $3 (BinaryExpression Or e $2) }
  |                                    { id }

AndExpression :: { Expression }
  : RelationalExpression AndExpressionSuffix 
                                       { $2 $1 }

AndExpressionSuffix :: { Expression -> Expression }
  : AmpersandAmpersand RelationalExpression AndExpressionSuffix 
                                       { \e -> $3 (BinaryExpression And e $2) }
  |                                    { id }

RelationalExpression :: { Expression }
  : AdditiveExpression RelationalExpressionSuffix 
                                       { $2 $1 }

RelationalExpressionSuffix :: { Expression -> Expression }
  : RelationalOp AdditiveExpression    { \e -> BinaryExpression $1 e $2 }
  |                                    { id }

RelationalOp :: { BinaryOp }
  : EqualEqual                         { Equal }
  | BangEqual                          { NotEqual }
  | LessEqual                          { LessEqual }
  | LessThan                           { LessThan }
  | GreaterEqual                       { GreaterEqual }
  | GreaterThan                        { GreaterThan }

AdditiveExpression :: { Expression }
  : MultiplicativeExpression AdditiveExpressionSuffix 
                                       { $2 $1 }

AdditiveExpressionSuffix :: { Expression -> Expression }
  : AdditiveOp MultiplicativeExpression AdditiveExpressionSuffix 
                                       { \e -> $3 (BinaryExpression $1 e $2) }
  |                                    { id }

AdditiveOp :: { BinaryOp }
  : Plus                               { Plus }
  | Minus                              { Minus }

MultiplicativeExpression :: { Expression }
  : Factor MultiplicativeExpressionSuffix 
                                       { $2 $1 }

MultiplicativeExpressionSuffix :: { Expression -> Expression }
  : MultiplicativeOp Factor MultiplicativeExpressionSuffix 
                                       { \e -> $3 (BinaryExpression $1 e $2) }
  |                                    { id }

MultiplicativeOp :: { BinaryOp }
  : Star { Times }
  | Slash { Divide }

Factor :: { Expression }
  : LiteralInt                         { LiteralValue $ LiteralInt (position $1) (lexeme $1) }
  | LiteralFloat                       { LiteralValue $ LiteralFloat (position $1) (lexeme $1) }
  | LiteralString                      { LiteralValue $ LiteralString (position $1) (lexeme $1)}
  | True                               { LiteralValue $ LiteralBool (position $1) True }
  | False                              { LiteralValue $ LiteralBool (position $1) False }
  | UnaryOperator Factor               { UnaryExpression (fst $1) (snd $1) $2 }
  | LParen Expression RParen           { Parenthesis (position $1 `combine` position $3) $2 }
  | Identifier OptionalParameters      { case $2 of
                                           Nothing -> IdentifierReference (mkIdentifier $1)
                                           Just a -> CallExpression (mkIdentifier $1) a }

UnaryOperator :: { (Position, UnaryOp) }
  : Bang                               { (position $1, UnaryNot) }
  | Plus                               { (position $1, UnaryPlus) }
  | Minus                              { (position $1, UnaryMinus) }

OptionalParameters :: { Maybe [Expression]}
  : LParen Arguments RParen            { Just $2 }
  |                                    { Nothing }

{

mkIdentifier (TIdentifier p l) = Identifier p l

parseError :: Token -> Lexer a
parseError t =
  throwError $ ParsingError (position t) $ SC.stringToText $ show t


parse :: Text -> Either Error Program
parse input =
  evalLexer parseProgram (input, startPoint)

}