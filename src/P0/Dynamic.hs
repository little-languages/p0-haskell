module P0.Dynamic
  ( parse
  ) where

import           Control.Monad.State
import           Control.Monad       (when)
import           Data.List           (find, isPrefixOf)
import qualified Data.Map            as Map
import qualified Data.Set            as Set
import qualified Data.Text           as Text

import           P0.Dynamic.TST
import qualified P0.Errors           as Errors
import qualified P0.Static           as Static
import           P0.Position         (Position (..), Positionable (..), combine,
                                      position)
import qualified P0.Static.AST       as AST
import qualified P0.StringConversion as SC
import qualified P0.Type             as Type

data Binding
  = Variable Type.Type
  | Constant Type.Type
  | Function [Type.Type] (Maybe Type.Type)
  deriving (Show)

instance Type.TypeOfable Binding where
    typeOf (Constant t) = t
    typeOf (Variable t) = t
    typeOf t            = error $ "P0.Dynamic.bindingType: " <> show t

type Environment = Map.Map Text.Text Binding

type StateType = (Environment, [Errors.Error])

type TState a = State StateType a

defaultTState :: StateType
defaultTState = (Map.empty, [])

parse :: Text.Text -> Either [Errors.Error] Program
parse input =
  case Static.parse input of
    Left error -> Left [error]
    Right ast ->
      case translate' ast of
        (tst, (_, [])) -> Right tst
        (_, (_, xs))   -> Left xs
      where translate' p = runState (translateP p) defaultTState

translateP :: AST.Program -> TState Program
translateP (AST.Program ds) =
    let isMain (FunctionDeclaration "main" [] _ Nothing) = True
        isMain _
                                 = False
        retrieveMain ds' =
          case find isMain ds' of
            Just (FunctionDeclaration _ _ [s] Nothing) -> s
            Just (FunctionDeclaration _ _ s Nothing)   -> BlockStatement s
            _                                          -> EmptyStatement

        filterMain =
          filter (not . isMain)

        prepareBindings :: [AST.Declaration] -> TState ()
        prepareBindings ds =
          forM_ ds prepareBinding

        name :: AST.Declaration -> Text.Text
        name (AST.VariableDeclaration _ (AST.Identifier _ n) _) = n
        name (AST.FunctionDeclaration (AST.Identifier _ n) _ _ _) = n

        prepareBinding :: AST.Declaration -> TState ()
        prepareBinding d = do
          let n' = name d
          binding <- lookupBinding n'
          Control.Monad.when (n' == "main")
            $ case d of
                (AST.FunctionDeclaration (AST.Identifier _ _) [] _ Nothing)
                  -> return ()
                _ -> reportError $ Errors.InvalidDeclarationOfMain (position d)
          case binding of
            Nothing -> case d of
                         (AST.VariableDeclaration AST.ReadOnly (AST.Identifier _ n) le) -> includeBinding n $ Constant $ Type.typeOf le
                         (AST.VariableDeclaration AST.ReadWrite (AST.Identifier _ n) le) -> includeBinding n $ Variable $ Type.typeOf le
                         (AST.FunctionDeclaration (AST.Identifier _ n) args _ Nothing) -> includeBinding n $ Function (map snd args) Nothing
                         (AST.FunctionDeclaration (AST.Identifier _ n) args _ (Just (typ, _))) -> includeBinding n $ Function (map snd args) (Just typ)
            Just _ -> reportError $ Errors.AttemptToRedefineDeclaration (position d) n'
     in do prepareBindings ds
           ds' <- mapM translateD ds
           return (Program (filterMain ds') (retrieveMain ds'))

translateD :: AST.Declaration -> TState Declaration
translateD (AST.VariableDeclaration AST.ReadOnly (AST.Identifier _ id) e) = do
  e' <- translateLE e
  return $ ConstantDeclaration id e'
translateD (AST.VariableDeclaration AST.ReadWrite (AST.Identifier _ id) e) = do
  e' <- translateLE e
  return $ VariableDeclaration id e'
translateD (AST.FunctionDeclaration (AST.Identifier _ id) args ss fe) =
  let uniqueArgumentNames :: [(AST.Identifier, Type.Type)] -> TState ()
      uniqueArgumentNames args =
          let uniqueName names (AST.Identifier p n, _) =
                if Set.member n names
                  then do
                      reportError $ Errors.AttemptToRedefineDeclaration p n
                      return names
                  else return $ Set.insert n names
          in
            foldM_ uniqueName Set.empty args

      includeArgumentBindings :: [(AST.Identifier, Type.Type)] -> TState ()
      includeArgumentBindings =
        mapM_ (\(AST.Identifier _ n, t) -> includeBinding n (Variable t))

      mapArguments :: [(AST.Identifier, Type.Type)] -> [(Text.Text, Type.Type)]
      mapArguments args =
        map (\(AST.Identifier _ n, t) -> (n, t)) args

      translateFunctionExpression :: Maybe (Type.Type, AST.Expression) -> TState (Maybe Expression)
      translateFunctionExpression Nothing = return Nothing
      translateFunctionExpression (Just (_, e)) = do
        e' <- translateE e
        return $ Just e'
   in do
     uniqueArgumentNames args
     env' <- environment
     includeArgumentBindings args
     ss' <- translateSS ss
     fe' <- translateFunctionExpression fe
     setEnvironment env'
     return $ FunctionDeclaration id (mapArguments args) ss' fe'

translateSS :: [AST.Statement] -> TState [Statement]
translateSS ss =
    let uniqueName names (AST.AssignmentStatement (Just _) (AST.Identifier p n) _) =
            if Set.member n names
                then do
                    reportError $ Errors.AttemptToRedefineDeclaration p n
                    return names
                else return $ Set.insert n names
        uniqueName names _ = return names
    in do
      foldM_ uniqueName Set.empty ss
      mapM translateS ss

translateS :: AST.Statement -> TState Statement
translateS (AST.AssignmentStatement Nothing (AST.Identifier p n) e) = do
  binding <- lookupBinding n
  e' <- translateE e
  case binding of
    Nothing -> reportError $ Errors.UnknownIdentifier p n
    Just (Variable t') -> conditionalError_ (e' `compatibleWithType` t') $ Errors.UnableToAssignIncompatibleTypes p t' (position e) (Type.typeOf e')
    Just (Constant _) -> reportError $ Errors.UnableToAssignToConstant p n
    Just _ -> reportError $ Errors.UnableToAssignToFunction p n
  return $ AssignmentStatement n e'
translateS (AST.AssignmentStatement (Just AST.ReadOnly) (AST.Identifier _ n) e) = do
  e' <- translateE e
  includeBinding n $ Constant $ Type.typeOf e'
  return $ ConstantDeclarationStatement n e'
translateS (AST.AssignmentStatement (Just AST.ReadWrite) (AST.Identifier _ n) e) = do
  e' <- translateE e
  includeBinding n $ Variable $ Type.typeOf e'
  return $ VariableDeclarationStatement n e'
translateS (AST.IfThenElseStatement e s Nothing) = do
  e' <- translateE e
  s' <- translateS s
  conditionalError_ (e' `compatibleWithType` Type.Bool) $ Errors.IfGuardNotBoolean (position e) (Type.typeOf e')
  return $ IfThenElseStatement e' s' Nothing
translateS (AST.IfThenElseStatement e s1 (Just s2)) = do
  e' <- translateE e
  s1' <- translateS s1
  s2' <- translateS s2
  conditionalError_ (e' `compatibleWithType` Type.Bool) $ Errors.IfGuardNotBoolean (position e) (Type.typeOf e')
  return $ IfThenElseStatement e' s1' $ Just s2'
translateS (AST.WhileStatement e s) = do
  e' <- translateE e
  s' <- translateS s
  conditionalError_ (e' `compatibleWithType` Type.Bool) $ Errors.WhileGuardNotBoolean (position e) (Type.typeOf e')
  return $ WhileStatement e' s'
translateS (AST.BlockStatement ss) = do
  env' <- environment
  ss' <- translateSS ss
  setEnvironment env'
  return $ BlockStatement ss'
translateS (AST.CallStatement (AST.Identifier p n) args) = do
  binding <- lookupBinding n
  args' <- mapM translateE args
  case binding of
    Nothing ->
      if n == "print" || n == "println"
        then return ()
        else reportError $ Errors.UnknownIdentifier p n
    Just (Function parameters Nothing) -> do
      forM_
        (zip3 args args' parameters)
        (\(a, a', p) -> conditionalError (a' `compatibleWithType` p) $ Errors.IncompatibleArgumentType (position a) p (Type.typeOf a'))
      conditionalError_ (length parameters == length args) $ Errors.MismatchInNumberOfParameters p (length parameters) (length args)
    Just (Function _ _) -> reportError $ Errors.UnableToCallValueFunctionAsUnitFunction p n
    Just (Variable t) -> reportError $ Errors.UnableToCallVariableAsFunction p n t
    Just (Constant t) -> reportError $ Errors.UnableToCallConstantAsFunction p n t
  return $ CallStatement n args'
translateS AST.EmptyStatement = return EmptyStatement

translateE :: AST.Expression -> TState Expression
translateE e@(AST.TernaryExpression e1 e2 e3) = do
  e1' <- translateE e1
  e2' <- translateE e2
  e3' <- translateE e3
  conditionalError_ (e1' `compatibleWithType` Type.Bool) $ Errors.TernaryExpressionNotBoolean (position e) (position e1)
  conditionalError_ (e2' `compatibleTypes` e3') $ Errors.TernaryExpressionResultIncompatible (position e2) (position e3)
  return $ TernaryExpression e1' e2' e3'
translateE (AST.BinaryExpression op e1 e2)
  | op `elem` [AST.And, AST.Or] = do
    e1' <- translateE e1
    e2' <- translateE e2
    let op' = translateBO op
    conditionalError_ (e1' `compatibleWithType` Type.Bool) $ Errors.BinaryExpressionRequiresOperandType op' (position e1) (Type.typeOf e1')
    conditionalError_ (e2' `compatibleWithType` Type.Bool) $ Errors.BinaryExpressionRequiresOperandType op' (position e2) (Type.typeOf e2')
    return $ BinaryExpression op' e1' e2'
  | op `elem` [AST.Equal, AST.NotEqual] = do
    e1' <- translateE e1
    e2' <- translateE e2
    let op' = translateBO op
    conditionalError_ (e1' `compatibleTypes` e2') $
      Errors.BinaryExpressionOperandsIncompatible op' (position e1) (Type.typeOf e1') (position e2) (Type.typeOf e2')
    return $ BinaryExpression op' e1' e2'
  | op `elem` [AST.Plus, AST.Minus, AST.Times, AST.Divide, AST.LessThan, AST.LessEqual, AST.GreaterThan, AST.GreaterEqual] = do
    e1' <- translateE e1
    e2' <- translateE e2
    let op' = translateBO op
    conditionalError_ (e1' `compatibleWithTypes` [Type.Int, Type.Float]) $ Errors.BinaryExpressionRequiresOperandType op' (position e1) (Type.typeOf e1')
    conditionalError_ (e2' `compatibleWithTypes` [Type.Int, Type.Float]) $ Errors.BinaryExpressionRequiresOperandType op' (position e2) (Type.typeOf e2')
    conditionalError_ (e1' `compatibleTypes` e2') $
      Errors.BinaryExpressionOperandsIncompatible op' (position e1) (Type.typeOf e1') (position e2) (Type.typeOf e2')
    return $ BinaryExpression op' e1' e2'
  | otherwise =
    error $ "P0.Dynamics.Translatable AST.Expression Expression: AST.BinaryExpression: " <> show op <> ", " <> show e1 <> ", " <> show e2
translateE (AST.LiteralValue v) = do
    v' <- translateLV v
    return $ LiteralValue v'
translateE (AST.UnaryExpression p1 AST.UnaryMinus (AST.LiteralValue (AST.LiteralInt p2 v))) = do
    value <- stringToInt' (p1 `combine` p2) $ "-" `Text.append` v
    return $ LiteralValue value
translateE (AST.UnaryExpression p1 AST.UnaryMinus (AST.LiteralValue (AST.LiteralFloat p2 v))) = do
    value <- stringToFloat' (p1 `combine` p2) $ "-" `Text.append` v
    return $ LiteralValue value
translateE (AST.UnaryExpression _ AST.UnaryNot e) = do
    e' <- translateE e
    conditionalError_ (e' `compatibleWithType` Type.Bool) $ Errors.UnaryExpressionRequiresOperandType UnaryNot (position e) (Type.typeOf e')
    return $ UnaryExpression UnaryNot e'
translateE (AST.UnaryExpression _ op e) = do
    e' <- translateE e
    let op' = translateUO op
    conditionalError_ (e' `compatibleWithTypes` [Type.Int, Type.Float]) $ Errors.UnaryExpressionRequiresOperandType op' (position e) (Type.typeOf e')
    return $ UnaryExpression op' e'
translateE (AST.IdentifierReference (AST.Identifier p id)) = do
    binding <- lookupBinding id
    case binding of
      Nothing -> do
        reportError $ Errors.UnknownIdentifier p id
        return $ IdentifierReference Type.Error id
      Just (Function _ _) -> do
        reportError $ Errors.UnableToReferenceFunction p id
        return $ IdentifierReference Type.Error id
      Just b -> return $ IdentifierReference (Type.typeOf b) id
translateE (AST.CallExpression (AST.Identifier p n) args) = do
    binding <- lookupBinding n
    args' <- mapM translateE args
    case binding of
      Nothing -> do
        reportError $ Errors.UnknownIdentifier p n
        return $ IdentifierReference Type.Error n
      Just (Function _ Nothing) -> do
        reportError $ Errors.UnableToCallUnitFunctionAsValueFunction p n
        return $ IdentifierReference Type.Error n
      Just (Function parameters (Just t)) -> do
        forM_
          (zip3 args args' parameters)
          (\(a, a', p) -> conditionalError (a' `compatibleWithType` p) $ Errors.IncompatibleArgumentType (position a) p (Type.typeOf a'))
        conditionalError_ (length parameters == length args) $ Errors.MismatchInNumberOfParameters p (length parameters) (length args)
        return $ CallExpression t n args'
      Just (Variable t) -> do
        reportError $ Errors.UnableToCallVariableAsFunction p n t
        return $ IdentifierReference Type.Error n
      Just (Constant t) -> do
        reportError $ Errors.UnableToCallConstantAsFunction p n t
        return $ IdentifierReference Type.Error n
translateE (AST.Parenthesis _ e) = translateE e

translateLE :: AST.LiteralExpression -> TState LiteralValue
translateLE (AST.LiteralExpressionValue v) = translateLV v
translateLE (AST.LiteralExpressionUnaryValue _ AST.UnaryPlus v) = translateLV v
translateLE (AST.LiteralExpressionUnaryValue p1 AST.UnaryMinus (AST.LiteralInt p2 v)) = stringToInt' (p1 `combine` p2) $ "-" `Text.append` v
translateLE (AST.LiteralExpressionUnaryValue p1 AST.UnaryMinus (AST.LiteralFloat p2 v)) = stringToFloat' (p1 `combine` p2) $ "-" `Text.append` v
translateLE e = error $ "P0.Dynamic.Translatable AST.LiteralExpression LiteralValue: " <> show e

translateLV :: AST.LiteralValue -> TState LiteralValue
translateLV (AST.LiteralBool _ v) = return $ LiteralBool v
translateLV (AST.LiteralInt p v) = stringToInt' p v
translateLV (AST.LiteralFloat p v) = stringToFloat' p v
translateLV (AST.LiteralString _ s) = do
    let s' = unescape $ dropRight 1 $ drop 1 $ SC.textToString s
    return $ LiteralString $ SC.stringToText s'
    where
      dropRight n s = take (length s - n) s
      unescape []             = ""
      unescape ('\\':'\\':cs) = '\\' : cs
      unescape ('\\':'"':cs)  = '"' : cs
      unescape (c:cs)         = c : unescape cs

compatibleTypes :: Expression -> Expression -> Bool
compatibleTypes e1 e2 =
  case (Type.typeOf e1, Type.typeOf e2) of
    (Type.Error, _) -> True
    (_, Type.Error) -> True
    (t1, t2)   -> t1 == t2

compatibleWithType :: Expression -> Type.Type -> Bool
compatibleWithType e = compatibleWithType' (Type.typeOf e)

compatibleWithType' :: Type.Type -> Type.Type -> Bool
compatibleWithType' t1 t2 =
  case t1 of
    Type.Error -> True
    t'    -> t' == t2

compatibleWithTypes :: Expression -> [Type.Type] -> Bool
compatibleWithTypes e = compatibleWithTypes' (Type.typeOf e)
  where
    compatibleWithTypes' Type.Error _ = True
    compatibleWithTypes' t ts    = t `elem` ts

stringToInt' :: Position -> Text.Text -> TState LiteralValue
stringToInt' p s =
  if -2147483648 <= value && value <= 2147483647
    then return $ LiteralInt (fromIntegral value :: Int)
    else do
      reportError $ Errors.LiteralIntOverflow p s
      return $ LiteralInt 0
  where
    value :: Integer
    value = read $ SC.textToString s

stringToFloat' :: Position -> Text.Text -> TState LiteralValue
stringToFloat' p s =
  if isInfinite value
    then do
      reportError $ Errors.LiteralFloatOverflow p s
      return $ LiteralFloat 0.0
    else return $ LiteralFloat value
  where
    value :: Float
    value = read (s' $ SC.textToString s)
    s' s
      | "." `isPrefixOf` s = "0" ++ s
      | "-." `isPrefixOf` s = "-0" ++ drop 1 s
      | otherwise = s

translateBO :: AST.BinaryOp -> BinaryOp
translateBO AST.And          = And
translateBO AST.Or           = Or
translateBO AST.Equal        = Equal
translateBO AST.NotEqual     = NotEqual
translateBO AST.LessThan     = LessThan
translateBO AST.LessEqual    = LessEqual
translateBO AST.GreaterThan  = GreaterThan
translateBO AST.GreaterEqual = GreaterEqual
translateBO AST.Plus         = Plus
translateBO AST.Minus        = Minus
translateBO AST.Times        = Times
translateBO AST.Divide       = Divide

translateUO :: AST.UnaryOp -> UnaryOp
translateUO AST.UnaryPlus  = UnaryPlus
translateUO AST.UnaryMinus = UnaryMinus
translateUO AST.UnaryNot   = UnaryNot

reportError :: Errors.Error -> TState ()
reportError e = modify (\(env, errs) -> (env, errs ++ [e]))

conditionalError :: Bool -> Errors.Error -> TState Bool
conditionalError condition e = do
  unless condition $ reportError e
  return condition

conditionalError_ :: Bool -> Errors.Error -> TState ()
conditionalError_ condition e = do
  _ <- conditionalError condition e
  return ()

includeBinding :: Text.Text -> Binding -> TState ()
includeBinding n b = modify (\(env, errs) -> (Map.insert n b env, errs))

lookupBinding :: Text.Text -> TState (Maybe Binding)
lookupBinding n = do
  (env, _) <- get
  return $ Map.lookup n env

environment :: TState Environment
environment = do
  (env, _) <- get
  return env

setEnvironment :: Environment -> TState ()
setEnvironment env =
  modify (\(_, errs) -> (env, errs))
