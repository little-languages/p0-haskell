module P0.Position
  ( combine
  , mkPosition
  , Point(..)
  , Position(..)
  , Positionable(..)
  , positionLength
  , startPoint
  , startPosition
  ) where

import qualified Data.Text      as Text
import qualified Data.Yaml      as Yaml

import           P0.PrettyPrint

data Position
  = SourcePosition Point Point
  | SourcePoint Point
  deriving (Eq)

data Point =
  Point Int Int Int
  deriving (Eq)

startPosition :: Position
startPosition = SourcePoint startPoint

startPoint :: Point
startPoint = Point 0 1 1

instance Show Position where
  show (SourcePosition p1 p2) = show p1 ++ "-" ++ show p2
  show (SourcePoint p)        = show p

instance Show Point where
  show (Point offset line column) = show offset ++ ":" ++ show line ++ ":" ++ show column

mkPosition :: Point -> Int -> Position
mkPosition p 0 = SourcePoint p
mkPosition p@(Point offset line column) length = SourcePosition p $ Point (offset + length') line (column + length')
  where
    length' = length - 1

combine :: Position -> Position -> Position
combine (SourcePoint p1) (SourcePoint p2) = SourcePosition (min p1 p2) (max p1 p2)
combine (SourcePoint p1) (SourcePosition p21 p22) = SourcePosition (min p1 p21) (max p1 p22)
combine (SourcePosition p11 p12) (SourcePoint p2) = SourcePosition (min p11 p2) (max p12 p2)
combine (SourcePosition p11 p12) (SourcePosition p21 p22) = SourcePosition (min p11 p21) (max p12 p22)

instance Ord Point where
  (<=) (Point o1 _ _) (Point o2 _ _) = o1 <= o2

positionLength :: Position -> Int
positionLength (SourcePosition (Point oS _ _) (Point oE _ _)) = oE - oS
positionLength (SourcePoint _)                                = 1

class Positionable p where
  position :: p -> Position

instance Yamlable Position where
  toYaml = Yaml.String . Text.pack . show
