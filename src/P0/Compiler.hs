{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecursiveDo           #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeSynonymInstances  #-}

module P0.Compiler
  ( parse
  ) where

import           Control.Monad.State
import qualified Data.Map                        as M
import           Data.Text                       (Text)

import           LLVM.AST                        (Operand)
import qualified LLVM.AST                        as AST
import qualified LLVM.AST.Constant               as C
import qualified LLVM.AST.Float                  as F
import qualified LLVM.AST.FloatingPointPredicate as FP
import qualified LLVM.AST.IntegerPredicate       as IP
import           LLVM.AST.Name
import           LLVM.AST.ParameterAttribute     (ParameterAttribute)
import qualified LLVM.AST.Type                   as AST

import qualified LLVM.IRBuilder.Constant         as L
import qualified LLVM.IRBuilder.Instruction      as L
import qualified LLVM.IRBuilder.Module           as L
import qualified LLVM.IRBuilder.Monad            as L

import qualified P0.Dynamic                      as Dynamic
import qualified P0.Dynamic.TST                  as TST
import qualified P0.Errors                       as Errors
import qualified P0.StringConversion             as SC
import qualified P0.Type                         as Type

data Env =
  Env
    { operands      :: M.Map Text Operand
    , printOperands :: M.Map Type.Type Operand
    , strings       :: M.Map Text Operand
    }
  deriving (Eq, Show)

type LLVM = L.ModuleBuilderT (State Env)

type Codegen = L.IRBuilderT LLVM

parse :: Text -> Either [Errors.Error] AST.Module
parse = fmap compile . Dynamic.parse

compile :: TST.Program -> AST.Module
compile (TST.Program ds s) =
  flip evalState (Env {operands = M.empty, printOperands = M.empty, strings = M.empty}) $
  L.buildModuleT "p0" $ do
    mapM_ emitBuiltIn builtIns
    mapM_ bindPrintType printTypeNames
    mapM_ cDeclaration ds
    emitMain s

emitBuiltIn :: (Text, [AST.Type], AST.Type) -> LLVM ()
emitBuiltIn (name, argumentTypes, returnType) = do
  func <- L.extern (mkName' name) argumentTypes returnType
  registerOperand name func

builtIns :: [(Text, [AST.Type], AST.Type)]
builtIns =
  [ ("_print_bool", [AST.i8], AST.void)
  , ("_print_int", [AST.i32], AST.void)
  , ("_print_string", [AST.ptr AST.i8], AST.void)
  , ("_print_float", [AST.float], AST.void)
  , ("_print_ln", [], AST.void)
  ]

bindPrintType :: (Type.Type, Text) -> LLVM ()
bindPrintType (typ, name) = do
  op <- gets $ (M.! name) . operands
  modify $ \env -> env {printOperands = M.insert typ op (printOperands env)}

printTypeNames :: [(Type.Type, Text)]
printTypeNames = [(Type.Bool, "_print_bool"), (Type.Int, "_print_int"), (Type.String, "_print_string"), (Type.Float, "_print_float")]

cDeclaration :: TST.Declaration -> LLVM ()
cDeclaration (TST.VariableDeclaration n v) = emitToplevelVariableDeclaration n v
cDeclaration (TST.ConstantDeclaration n v) = emitToplevelVariableDeclaration n v
cDeclaration (TST.FunctionDeclaration n p ss (Just e)) =
  mdo registerOperand n f
      f <-
        do let returnType = typeToAST $ Type.typeOf e
           let params = map (\(n, t) -> (typeToAST t, L.ParameterName $ SC.textToSBS n)) p
           L.function (mkName' n) params returnType $ emitBody ss e p
      return ()
  where
    emitBody :: [TST.Statement] -> TST.Expression -> [(Text, Type.Type)] -> [AST.Operand] -> Codegen ()
    emitBody ss e ps ops =
      locally $ do
        _entry <- L.block `L.named` "entry"
        emmitFunctionArguments ps ops
        mapM_ cStatement ss
        e' <- cExpression e
        L.ret e'
cDeclaration (TST.FunctionDeclaration n p ss Nothing) =
  mdo registerOperand n f
      f <-
        do let params = map (\(n, t) -> (typeToAST t, L.ParameterName $ SC.textToSBS n)) p
           L.function (mkName' n) params AST.void $ emitBody ss p
      return ()
  where
    emitBody :: [TST.Statement] -> [(Text, Type.Type)] -> [AST.Operand] -> Codegen ()
    emitBody ss ps ops =
      locally $ do
        _entry <- L.block `L.named` "entry"
        emmitFunctionArguments ps ops
        mapM_ cStatement ss
        L.retVoid

emmitFunctionArguments :: [(Text, Type.Type)] -> [Operand] -> Codegen ()
emmitFunctionArguments ps ops =
  forM_ (zip ps ops) $ \((n, typ), op) -> do
    addr <- L.alloca (typeToAST typ) Nothing 0
    L.store addr 0 op
    registerOperand n addr

emitToplevelVariableDeclaration :: Text -> TST.LiteralValue -> LLVM ()
emitToplevelVariableDeclaration n v = do
  let v' = cLiteralValueConstant v
  let typ = typeToAST $ Type.typeOf v
  addr <- L.global (mkName' n) typ v'
  registerOperand n addr

typeToAST :: Type.Type -> AST.Type
typeToAST Type.Int   = AST.i32
typeToAST Type.Float = AST.float
typeToAST Type.Bool  = AST.i1
typeToAST t          = error $ "typeToAST: Unexpected type: " <> show t

emitMain :: TST.Statement -> LLVM ()
emitMain s = void $ L.function (mkName "main") [] AST.i32 emitBody
  where
    emitBody :: [AST.Operand] -> Codegen ()
    emitBody _ = do
      _entry <- L.block `L.named` "entry"
      cStatement s
      L.ret $ L.int32 0

cStatement :: TST.Statement -> Codegen ()
cStatement (TST.AssignmentStatement n e) = do
  e' <- cExpression e
  addr <- getOperand n
  L.store addr 0 e'
cStatement (TST.VariableDeclarationStatement n e) = emitDeclarationStatement n e
cStatement (TST.ConstantDeclarationStatement n e) = emitDeclarationStatement n e
cStatement (TST.IfThenElseStatement e s Nothing) =
  mdo e' <- cExpression e
      L.condBr e' thenBlock mergeBlock
      thenBlock <- L.block `L.named` "then"
      cStatement s
      L.br mergeBlock
      mergeBlock <- L.block `L.named` "merge"
      return ()
cStatement (TST.IfThenElseStatement e s1 (Just s2)) =
  mdo e' <- cExpression e
      L.condBr e' thenBlock elseBlock
      thenBlock <- L.block `L.named` "then"
      cStatement s1
      L.br mergeBlock
      elseBlock <- L.block `L.named` "else"
      cStatement s2
      L.br mergeBlock
      mergeBlock <- L.block `L.named` "merge"
      return ()
cStatement (TST.WhileStatement e s) =
  mdo L.br whileBlock
      whileBlock <- L.block `L.named` "while"
      e' <- cExpression e
      L.condBr e' bodyBlock mergeBlock
      bodyBlock <- L.block `L.named` "body"
      cStatement s
      L.br whileBlock
      mergeBlock <- L.block `L.named` "merge"
      return ()
cStatement (TST.BlockStatement ss) = locally $ mapM_ cStatement ss
cStatement (TST.CallStatement "println" es) = do
  emitPrintStatement es
  n <- gets $ (M.! "_print_ln") . operands
  call_ n []
cStatement (TST.CallStatement "print" es) = emitPrintStatement es
cStatement (TST.CallStatement n es) = do
  es' <- mapM cExpression es
  n' <- gets $ (M.! n) . operands
  void $ L.call n' $ map (, []) es'
cStatement TST.EmptyStatement = return ()

emitPrintStatement :: [TST.Expression] -> Codegen ()
emitPrintStatement es =
  let extendOperand Type.Bool op = L.zext op AST.i8
      extendOperand _ op         = return op
      writeExpression e = do
        let typeOfE = Type.typeOf e
        e' <- cExpression e
        e'' <- extendOperand typeOfE e'
        n <- gets $ (M.! typeOfE) . printOperands
        call_ n [(e'', [])]
   in mapM_ writeExpression es

emitDeclarationStatement :: Text -> TST.Expression -> Codegen ()
emitDeclarationStatement n e = do
  e' <- cExpression e
  let typ = typeToAST $ Type.typeOf e
  addr <- L.alloca typ Nothing 0
  L.store addr 0 e'
  registerOperand n addr

cExpression :: TST.Expression -> Codegen Operand
cExpression (TST.TernaryExpression e1 e2 e3) =
  mdo e1' <- cExpression e1
      L.condBr e1' thenBlock elseBlock
      thenBlock <- L.block `L.named` "then"
      e2' <- cExpression e2
      L.br mergeBlock
      elseBlock <- L.block `L.named` "else"
      e3' <- cExpression e3
      L.br mergeBlock
      mergeBlock <- L.block `L.named` "merge"
      L.phi [(e2', thenBlock), (e3', elseBlock)]
cExpression (TST.BinaryExpression op e1 e2) = do
  e1' <- cExpression e1
  e2' <- cExpression e2
  case (op, Type.typeOf e1) of
    (TST.Equal, Type.Float)        -> L.fcmp FP.OEQ e1' e2'
    (TST.Equal, _)                 -> L.icmp IP.EQ e1' e2'
    (TST.NotEqual, Type.Float)     -> L.fcmp FP.ONE e1' e2'
    (TST.NotEqual, _)              -> L.icmp IP.NE e1' e2'
    (TST.LessThan, Type.Float)     -> L.fcmp FP.OLT e1' e2'
    (TST.LessThan, _)              -> L.icmp IP.SLT e1' e2'
    (TST.LessEqual, Type.Float)    -> L.fcmp FP.OLE e1' e2'
    (TST.LessEqual, _)             -> L.icmp IP.SLE e1' e2'
    (TST.GreaterThan, Type.Float)  -> L.fcmp FP.OGT e1' e2'
    (TST.GreaterThan, _)           -> L.icmp IP.SGT e1' e2'
    (TST.GreaterEqual, Type.Float) -> L.fcmp FP.OGE e1' e2'
    (TST.GreaterEqual, _)          -> L.icmp IP.SGE e1' e2'
    (TST.And, _)                   -> L.and e1' e2'
    (TST.Or, _)                    -> L.or e1' e2'
    (TST.Plus, Type.Float)         -> L.fadd e1' e2'
    (TST.Plus, _)                  -> L.add e1' e2'
    (TST.Minus, Type.Float)        -> L.fsub e1' e2'
    (TST.Minus, _)                 -> L.sub e1' e2'
    (TST.Times, Type.Float)        -> L.fmul e1' e2'
    (TST.Times, _)                 -> L.mul e1' e2'
    (TST.Divide, Type.Float)       -> L.fdiv e1' e2'
    (TST.Divide, _)                -> L.sdiv e1' e2'
cExpression (TST.UnaryExpression op e) = do
  e' <- cExpression e
  case (op, Type.typeOf e) of
    (TST.UnaryPlus, _)           -> return e'
    (TST.UnaryMinus, Type.Float) -> L.fsub (L.single 0.0) e'
    (TST.UnaryMinus, _)          -> L.sub (L.int32 0) e'
    (TST.UnaryNot, _)            -> L.xor e' (L.bit 1)
cExpression (TST.CallExpression _ n es) = do
  es' <- mapM cExpression es
  n' <- gets $ (M.! n) . operands
  L.call n' $ map (, []) es'
cExpression (TST.IdentifierReference _ n) = do
  op <- getOperand n
  L.load op 0
cExpression (TST.LiteralValue v) = cLiteralValue v

cLiteralValue :: TST.LiteralValue -> Codegen Operand
cLiteralValue (TST.LiteralString v) = do
  strings' <- gets strings
  case M.lookup v strings' of
    Nothing -> do
      let nm = mkName (show (M.size strings') <> ".str")
      op <- L.globalStringPtr (SC.textToString v) nm
      modify $ \env -> env {strings = M.insert v (AST.ConstantOperand op) strings'}
      return $ AST.ConstantOperand op
    Just op -> return op
cLiteralValue (TST.LiteralInt v) = return $ L.int32 (fromIntegral v)
cLiteralValue (TST.LiteralFloat v) = return $ L.single v
cLiteralValue (TST.LiteralBool v) = return $ L.bit $ booleanToBit v

cLiteralValueConstant :: TST.LiteralValue -> C.Constant
cLiteralValueConstant (TST.LiteralInt v) = C.Int 32 (fromIntegral v)
cLiteralValueConstant (TST.LiteralFloat v) = C.Float $ F.Single v
cLiteralValueConstant (TST.LiteralBool v) = C.Int 1 $ booleanToBit v
cLiteralValueConstant t = error $ "cLiteralValueConstant: unexpected: " <> show t

booleanToBit :: Bool -> Integer
booleanToBit True  = 1
booleanToBit False = 0

registerOperand :: MonadState Env m => Text -> Operand -> m ()
registerOperand name op = modify $ \env -> env {operands = M.insert name op (operands env)}

getOperand :: MonadState Env m => Text -> m Operand
getOperand n = do
  operands' <- gets operands
  unless (M.member n operands') $ do
    env <- get
    error $ "Unknown variable: " <> SC.textToString n <> ": " <> show env
  gets ((M.! n) . operands)

locally :: MonadState Env m => m a -> m a
locally computation = do
  oldState <- get
  result <- computation
  strings' <- gets strings
  put oldState
  modify (\env -> env {strings = strings'})
  return result

call_ :: L.MonadIRBuilder m => Operand -> [(Operand, [ParameterAttribute])] -> m ()
call_ op args = void $ L.call op args

mkName' :: Text -> Name
mkName' = mkName . SC.textToString
