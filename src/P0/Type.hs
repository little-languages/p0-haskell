module P0.Type where

import qualified P0.PrettyPrint      as PP
import qualified P0.StringConversion as SC

data Type
  = Int
  | Float
  | Bool
  | String
  | Error
  deriving (Eq, Ord)

class TypeOfable a where
  typeOf :: a -> Type

instance Show Type where
  show Int = "Int"
  show Float = "Float"
  show Bool = "Bool"
  show String = "String"
  show Error = "Error"

instance PP.Yamlable Type where
  toYaml t = PP.toString $ SC.stringToText $ show t
