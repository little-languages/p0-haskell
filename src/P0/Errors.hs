module P0.Errors
  ( Error(..)
  ) where

import           Data.Text           (Text)

import qualified P0.Dynamic.TST      as TST
import           P0.Position         (Position)
import           P0.PrettyPrint
import qualified P0.StringConversion as SC
import qualified P0.Type             as Type

data Error
  = AttemptToRedefineDeclaration Position Text
  | BinaryExpressionOperandsIncompatible TST.BinaryOp Position Type.Type Position Type.Type
  | BinaryExpressionRequiresOperandType TST.BinaryOp Position Type.Type
  | FunctionReturnTypeMismatch Position Text Type.Type
  | IfGuardNotBoolean Position Type.Type
  | IncompatibleArgumentType Position Type.Type Type.Type
  | InvalidDeclarationOfMain Position
  | InvalidFunctionReturn Position Type.Type Type.Type
  | LexicalError Position Text
  | LiteralIntOverflow Position Text
  | LiteralFloatOverflow Position Text
  | MismatchInNumberOfParameters Position Int Int
  | ParsingError Position Text
  | TernaryExpressionNotBoolean Position Position
  | TernaryExpressionResultIncompatible Position Position
  | UnableToAssignIncompatibleTypes Position Type.Type Position Type.Type
  | UnableToAssignToConstant Position Text
  | UnableToAssignToFunction Position Text
  | UnableToCallConstantAsFunction Position Text Type.Type
  | UnableToCallUnitFunctionAsValueFunction Position Text
  | UnableToCallValueFunctionAsUnitFunction Position Text
  | UnableToCallVariableAsFunction Position Text Type.Type
  | UnableToReferenceFunction Position Text
  | UnaryExpressionRequiresOperandType TST.UnaryOp Position Type.Type
  | UnknownIdentifier Position Text
  | WhileGuardNotBoolean Position Type.Type
  deriving (Eq, Show)

instance Yamlable Error where
  toYaml (LexicalError _ t) = toString $ "Lexical Error: " <> t
  toYaml (ParsingError _ t) = toString $ "Parsing Error: " <> t
  toYaml (AttemptToRedefineDeclaration p n) = toMap' "AttemptToRedefineDeclaration" [("position", toYaml p), ("name", toString n)]
  toYaml (LiteralIntOverflow p t) = toMap' "LiteralIntOverflow" [("position", toYaml p), ("text", toString t)]
  toYaml (LiteralFloatOverflow p t) = toMap' "LiteralFloatOverflow" [("position", toYaml p), ("text", toString t)]
  toYaml (TernaryExpressionNotBoolean p1 p2) = toMap' "TernaryExpressionNotBoolean" [("position", toYaml p1), ("boolPosition", toYaml p2)]
  toYaml (TernaryExpressionResultIncompatible p1 p2) =
    toMap' "TernaryExpressionResultIncompatible" [("thenPosition", toYaml p1), ("elsePosition", toYaml p2)]
  toYaml (BinaryExpressionRequiresOperandType op p t) =
    toMap' "BinaryExpressionRequiresOperandType" [("op", toYaml op), ("position", toYaml p), ("type", toYaml t)]
  toYaml (BinaryExpressionOperandsIncompatible op p1 t1 p2 t2) =
    toMap'
      "BinaryExpressionOperandsIncompatible"
      [("op", toYaml op), ("position1", toYaml p1), ("type1", toYaml t1), ("position2", toYaml p2), ("type2", toYaml t2)]
  toYaml (UnaryExpressionRequiresOperandType op p t) =
    toMap' "UnaryExpressionRequiresOperandType" [("op", toYaml op), ("position", toYaml p), ("type", toYaml t)]
  toYaml (UnknownIdentifier p n) = toMap' "UnknownIdentifier" [("position", toYaml p), ("name", toString n)]
  toYaml (UnableToReferenceFunction p n) = toMap' "UnableToReferenceFunction" [("position", toYaml p), ("name", toString n)]
  toYaml (FunctionReturnTypeMismatch p n t) = toMap' "FunctionReturnTypeMismatch" [("position", toYaml p), ("name", toString n), ("type", toYaml t)]
  toYaml (UnableToCallVariableAsFunction p n _) = toMap' "UnableToCallVariableAsFunction" [("position", toYaml p), ("name", toString n)]
  toYaml (UnableToCallConstantAsFunction p n _) = toMap' "UnableToCallConstantAsFunction" [("position", toYaml p), ("name", toString n)]
  toYaml (IncompatibleArgumentType p t1 t2) =
    toMap' "IncompatibleArgumentType" [("position", toYaml p), ("parameterType", toYaml t1), ("argumentType", toYaml t2)]
  toYaml (MismatchInNumberOfParameters p param args) =
    toMap'
      "MismatchInNumberOfParameters"
      [("position", toYaml p), ("parameters", toString $ SC.stringToText $ show param), ("arguments", toString $ SC.stringToText $ show args)]
  toYaml (UnableToAssignIncompatibleTypes p1 t1 p2 t2) =
    toMap'
      "UnableToAssignIncompatibleTypes"
      [("position", toYaml p1), ("type", toYaml t1), ("expressionPosition", toYaml p2), ("expressionType", toYaml t2)]
  toYaml (UnableToAssignToConstant p n) = toMap' "UnableToAssignToConstant" [("position", toYaml p), ("name", toString n)]
  toYaml (UnableToAssignToFunction p n) = toMap' "UnableToAssignToFunction" [("position", toYaml p), ("name", toString n)]
  toYaml (IfGuardNotBoolean p t) = toMap' "IfGuardNotBoolean" [("position", toYaml p), ("type", toYaml t)]
  toYaml (WhileGuardNotBoolean p t) = toMap' "WhileGuardNotBoolean" [("position", toYaml p), ("type", toYaml t)]
  toYaml (InvalidFunctionReturn p t1 t2) =
    toMap' "InvalidFunctionReturn" [("position", toYaml p), ("expectedType", toYaml t1), ("actualType", toYaml t2)]
  toYaml (UnableToCallUnitFunctionAsValueFunction p n) =
    toMap' "UnableToCallUnitFunctionAsValueFunction" [("position", toYaml p), ("name", toString n)]
  toYaml (UnableToCallValueFunctionAsUnitFunction p n) =
    toMap' "UnableToCallValueFunctionAsUnitFunction" [("position", toYaml p), ("name", toString n)]
  toYaml (InvalidDeclarationOfMain p) = toMap' "InvalidDeclarationOfMain" [("position", toYaml p)]
