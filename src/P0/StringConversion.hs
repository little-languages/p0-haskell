module P0.StringConversion where

import           Data.ByteString          (ByteString)
import           Data.ByteString.Internal (unpackChars)
import qualified Data.String              as String (fromString)
import qualified Data.Text                as Text
import qualified Data.Text.Lazy           as DTL
import qualified LLVM.Prelude             as Prelude (ShortByteString)

bsToString :: ByteString -> String
bsToString = unpackChars

dtlToString :: DTL.Text -> String
dtlToString = DTL.unpack

stringToSBS :: String -> Prelude.ShortByteString
stringToSBS s = textToSBS $ Text.pack s

stringToText :: String -> Text.Text
stringToText = Text.pack

textToSBS :: Text.Text -> Prelude.ShortByteString
textToSBS t = String.fromString $ Text.unpack t

textToString :: Text.Text -> String
textToString = Text.unpack
