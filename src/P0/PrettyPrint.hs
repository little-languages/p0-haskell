module P0.PrettyPrint where

import           Data.ByteString.UTF8 (ByteString)
import qualified Data.HashMap.Strict  as Map
import qualified Data.Text            as Text
import qualified Data.Vector          as Vector
import qualified Data.Yaml            as Yaml
import qualified Data.Yaml.Pretty     as YamlPretty

import qualified P0.StringConversion  as SC

valueToText :: Yaml.Value -> Text.Text
valueToText (Yaml.Object o) = SC.stringToText $ show o
valueToText (Yaml.Array a)  = SC.stringToText $ show a
valueToText (Yaml.String t) = t
valueToText (Yaml.Number s) = SC.stringToText $ show s
valueToText (Yaml.Bool b)   = SC.stringToText $ show b
valueToText Yaml.Null       = ""

class Yamlable y where
  toYaml :: y -> Yaml.Value

instance Yamlable o => Yamlable (Maybe o) where
  toYaml Nothing  = toString "Nothing"
  toYaml (Just o) = toMap [("Just", toYaml o)]

toArray :: [Yaml.Value] -> Yaml.Value
toArray = Yaml.Array . Vector.fromList

toArray' :: Yamlable y => [y] -> Yaml.Value
toArray' = toArray . map toYaml

toMap :: [(String, Yaml.Value)] -> Yaml.Value
toMap = Yaml.Object . Map.fromList . map (\(k, v) -> (Text.pack k, v))

toMap' :: String -> [(String, Yaml.Value)] -> Yaml.Value
toMap' n s = toMap [(n, toMap s)]

toString :: Text.Text -> Yaml.Value
toString = Yaml.String

pp :: Yaml.Value -> ByteString
pp = YamlPretty.encodePretty YamlPretty.defConfig
