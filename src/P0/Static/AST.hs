module P0.Static.AST
  ( BinaryOp(..)
  , Declaration(..)
  , Expression(..)
  , Identifier(..)
  , LiteralExpression(..)
  , LiteralValue(..)
  , Program(..)
  , Statement(..)
  , UnaryOp(..)
  , VariableAccess(..)
  ) where

import           Data.Text           (Text)

import           P0.Position         (Position (..), Positionable (..), combine)
import           P0.PrettyPrint
import qualified P0.StringConversion as SC
import qualified P0.Type             as Type

newtype Program =
  Program [Declaration]
  deriving (Show)

data Declaration
  = FunctionDeclaration Identifier [(Identifier, Type.Type)] [Statement] (Maybe (Type.Type, Expression))
  | VariableDeclaration VariableAccess Identifier LiteralExpression
  deriving (Show)

instance Positionable Declaration where
  position (FunctionDeclaration (Identifier p _) _ _ _) = p
  position (VariableDeclaration _ (Identifier p _) _) = p

data VariableAccess
  = ReadOnly
  | ReadWrite
  deriving (Show)

data Statement
  = AssignmentStatement (Maybe VariableAccess) Identifier Expression
  | IfThenElseStatement Expression Statement (Maybe Statement)
  | WhileStatement Expression Statement
  | BlockStatement [Statement]
  | CallStatement Identifier [Expression]
  | EmptyStatement
  deriving (Show)

data Expression
  = TernaryExpression Expression Expression Expression
  | BinaryExpression BinaryOp Expression Expression
  | UnaryExpression Position UnaryOp Expression
  | CallExpression Identifier [Expression]
  | IdentifierReference Identifier
  | Parenthesis Position Expression
  | LiteralValue LiteralValue
  deriving (Show)

instance Positionable Expression where
  position (TernaryExpression e1 _ e3) = position e1 `combine` position e3
  position (BinaryExpression _ e1 e2) = position e1 `combine` position e2
  position (UnaryExpression p _ e) = p `combine` position e
  position (CallExpression id args) = foldl combine (position id) $ map position args
  position (IdentifierReference identifier) = position identifier
  position (Parenthesis pos _) = pos
  position (LiteralValue v) = position v

data LiteralExpression
  = LiteralExpressionValue LiteralValue
  | LiteralExpressionUnaryValue Position UnaryOp LiteralValue
  deriving (Show)

instance Positionable LiteralExpression where
  position (LiteralExpressionValue v)          = position v
  position (LiteralExpressionUnaryValue p _ v) = p `combine` position v

instance Type.TypeOfable LiteralExpression where
  typeOf (LiteralExpressionValue v)                 = Type.typeOf v
  typeOf (LiteralExpressionUnaryValue _ UnaryNot _) = Type.Bool
  typeOf (LiteralExpressionUnaryValue _ _ v)        = Type.typeOf v

data LiteralValue
  = LiteralBool Position Bool
  | LiteralInt Position Text
  | LiteralFloat Position Text
  | LiteralString Position Text
  deriving (Show)

instance Positionable LiteralValue where
  position (LiteralBool pos _)   = pos
  position (LiteralInt pos _)    = pos
  position (LiteralFloat pos _)  = pos
  position (LiteralString pos _) = pos

instance Type.TypeOfable LiteralValue where
  typeOf (LiteralBool _ _)   = Type.Bool
  typeOf (LiteralInt _ _)    = Type.Int
  typeOf (LiteralFloat _ _)  = Type.Float
  typeOf (LiteralString _ _) = Type.String

data BinaryOp
  = Divide
  | Minus
  | Plus
  | Times
  | Equal
  | GreaterEqual
  | GreaterThan
  | LessEqual
  | LessThan
  | NotEqual
  | And
  | Or
  deriving (Eq, Show)

data UnaryOp
  = UnaryNot
  | UnaryMinus
  | UnaryPlus
  deriving (Eq, Show)

data Identifier =
  Identifier Position Text
  deriving (Eq, Show)

instance Positionable Identifier where
  position (Identifier pos _) = pos

instance Yamlable Program where
  toYaml (Program d) = toMap [("Program", toArray' d)]

instance Yamlable Declaration where
  toYaml (FunctionDeclaration name args statements Nothing) =
    toMap'
      "FunctionDeclaration"
      [ ("identifier", toYaml name)
      , ("arguments", toArray $ map (\(i, t) -> toMap [("name", toYaml i), ("type", toYaml t)]) args)
      , ("s", toArray' statements)
      ]
  toYaml (FunctionDeclaration name args statements (Just (typ, e))) =
    toMap'
      "FunctionDeclaration"
      [ ("identifier", toYaml name)
      , ("arguments", toArray $ map (\(i, t) -> toMap [("name", toYaml i), ("type", toYaml t)]) args)
      , ("result", toYaml typ)
      , ("s", toArray' statements)
      , ("e", toYaml e)
      ]
  toYaml (VariableDeclaration access name expression) =
    toMap' "VariableDeclaration" [("access", toYaml access), ("identifier", toYaml name), ("e", toYaml expression)]

instance Yamlable VariableAccess where
  toYaml ReadOnly  = toString "ReadOnly"
  toYaml ReadWrite = toString "ReadWrite"

instance Yamlable Statement where
  toYaml (AssignmentStatement Nothing n e) = toMap' "AssignmentStatement" [("identifier", toYaml n), ("e", toYaml e)]
  toYaml (AssignmentStatement (Just a) n e) = toMap' "VariableDeclarationStatement" [("access", toYaml a), ("identifier", toYaml n), ("e", toYaml e)]
  toYaml (IfThenElseStatement e s1 Nothing) = toMap' "IfThenElseStatement" [("e", toYaml e), ("s1", toYaml s1)]
  toYaml (IfThenElseStatement e s1 (Just s2)) = toMap' "IfThenElseStatement" [("e", toYaml e), ("s1", toYaml s1), ("s2", toYaml s2)]
  toYaml (WhileStatement e s) = toMap' "WhileStatement" [("e", toYaml e), ("s", toYaml s)]
  toYaml (BlockStatement s) = toMap [("BlockStatement", toArray' s)]
  toYaml (CallStatement n e) = toMap' "CallStatement" [("identifier", toYaml n), ("parameters", toArray' e)]
  toYaml EmptyStatement = toMap' "EmptyStatement" []

instance Yamlable Identifier where
  toYaml (Identifier p n) = toMap [("position", toYaml p), ("value", toString n)]

instance Yamlable Expression where
  toYaml (TernaryExpression e1 e2 e3) = toMap' "TernaryExpression" [("e1", toYaml e1), ("e2", toYaml e2), ("e3", toYaml e3)]
  toYaml (BinaryExpression op e1 e2) = toMap' "BinaryExpression" [("op", toYaml op), ("e1", toYaml e1), ("e2", toYaml e2)]
  toYaml (UnaryExpression p op e) = toMap' "UnaryExpression" [("position", toYaml p), ("op", toYaml op), ("e", toYaml e)]
  toYaml (CallExpression n ps) = toMap' "CallExpression" [("name", toYaml n), ("parameters", toArray' ps)]
  toYaml (IdentifierReference id) = toMap [("IdentifierReference", toYaml id)]
  toYaml (LiteralValue v) = toMap' "LiteralValue" [("value", toYaml v)]
  toYaml (Parenthesis p e) = toMap' "Parenthesis" [("position", toYaml p), ("e", toYaml e)]

instance Yamlable LiteralExpression where
  toYaml (LiteralExpressionValue v) = toMap' "LiteralExpressionValue" [("value", toYaml v)]
  toYaml (LiteralExpressionUnaryValue p op v) = toMap' "LiteralExpressionUnaryValue" [("position", toYaml p), ("op", toYaml op), ("value", toYaml v)]

instance Yamlable LiteralValue where
  toYaml (LiteralBool p v) = toMap' "LiteralBool" [("position", toYaml p), ("value", toString $ SC.stringToText $ show v)]
  toYaml (LiteralFloat p v) = toMap' "LiteralFloat" [("position", toYaml p), ("value", toString v)]
  toYaml (LiteralInt p v) = toMap' "LiteralInt" [("position", toYaml p), ("value", toString v)]
  toYaml (LiteralString p v) = toMap' "LiteralString" [("position", toYaml p), ("value", toString v)]

instance Yamlable BinaryOp where
  toYaml Divide       = toString "Divide"
  toYaml Minus        = toString "Minus"
  toYaml Plus         = toString "Plus"
  toYaml Times        = toString "Times"
  toYaml Equal        = toString "Equal"
  toYaml GreaterEqual = toString "GreaterEqual"
  toYaml GreaterThan  = toString "GreaterThan"
  toYaml LessEqual    = toString "LessEqual"
  toYaml LessThan     = toString "LessThan"
  toYaml NotEqual     = toString "NotEqual"
  toYaml And          = toString "And"
  toYaml Or           = toString "Or"

instance Yamlable UnaryOp where
  toYaml UnaryNot   = toString "UnaryNot"
  toYaml UnaryMinus = toString "UnaryMinus"
  toYaml UnaryPlus  = toString "UnaryPlus"
