{-# LANGUAGE MultiParamTypeClasses #-}

module P0.Dynamic.TST
  ( BinaryOp(..)
  , Declaration(..)
  , Expression(..)
  , LiteralValue(..)
  , Program(..)
  , Statement(..)
  , UnaryOp(..)
  ) where

import           Data.Text           (Text)

import           P0.PrettyPrint
import qualified P0.StringConversion as SC
import qualified P0.Type             as Type

data Program =
  Program [Declaration] Statement
  deriving (Show)

data Declaration
  = FunctionDeclaration Text [(Text, Type.Type)] [Statement] (Maybe Expression)
  | VariableDeclaration Text LiteralValue
  | ConstantDeclaration Text LiteralValue
  deriving (Show)

data LiteralValue
  = LiteralString Text
  | LiteralBool Bool
  | LiteralInt Int
  | LiteralFloat Float
  deriving (Show)

instance Type.TypeOfable LiteralValue where
  typeOf (LiteralString _) = Type.String
  typeOf (LiteralBool _)   = Type.Bool
  typeOf (LiteralInt _)    = Type.Int
  typeOf (LiteralFloat _)  = Type.Float

data Statement
  = AssignmentStatement Text Expression
  | VariableDeclarationStatement Text Expression
  | ConstantDeclarationStatement Text Expression
  | IfThenElseStatement Expression Statement (Maybe Statement)
  | WhileStatement Expression Statement
  | BlockStatement [Statement]
  | CallStatement Text [Expression]
  | EmptyStatement
  deriving (Show)

data Expression
  = TernaryExpression Expression Expression Expression
  | BinaryExpression BinaryOp Expression Expression
  | UnaryExpression UnaryOp Expression
  | CallExpression Type.Type Text [Expression]
  | IdentifierReference Type.Type Text
  | LiteralValue LiteralValue
  deriving (Show)

instance Type.TypeOfable Expression where
  typeOf (TernaryExpression _ e2 _) = Type.typeOf e2
  typeOf (BinaryExpression op e1 _)
    | op `elem` [Plus, Minus, Times, Divide] = Type.typeOf e1
    | otherwise = Type.Bool
  typeOf (UnaryExpression _ e)      = Type.typeOf e
  typeOf (CallExpression t _ _)       = t
  typeOf (IdentifierReference t _)    = t
  typeOf (LiteralValue l)             = Type.typeOf l

data BinaryOp
  = Divide
  | Minus
  | Plus
  | Times
  | Equal
  | GreaterEqual
  | GreaterThan
  | LessEqual
  | LessThan
  | NotEqual
  | And
  | Or
  deriving (Eq, Show)

data UnaryOp
  = UnaryNot
  | UnaryMinus
  | UnaryPlus
  deriving (Eq, Show)

instance Yamlable Program where
  toYaml (Program ds s) = toMap' "Program" [("d", toArray' ds), ("s", toYaml s)]

instance Yamlable Declaration where
  toYaml (FunctionDeclaration name args statements Nothing) =
    toMap'
      "FunctionDeclaration"
      [ ("identifier", toString name)
      , ("arguments", toArray $ map (\(i, t) -> toMap [("name", toString i), ("type", toYaml t)]) args)
      , ("s", toArray' statements)
      ]
  toYaml (FunctionDeclaration name args statements (Just e)) =
    toMap'
      "FunctionDeclaration"
      [ ("identifier", toString name)
      , ("arguments", toArray $ map (\(i, t) -> toMap [("name", toString i), ("type", toYaml t)]) args)
      , ("s", toArray' statements)
      , ("e", toYaml e)
      ]
  toYaml (ConstantDeclaration name expression) = toMap' "ConstantDeclaration" [("identifier", toString name), ("e", toYaml expression)]
  toYaml (VariableDeclaration name expression) = toMap' "VariableDeclaration" [("identifier", toString name), ("e", toYaml expression)]

instance Yamlable Statement where
  toYaml (AssignmentStatement n e) = toMap' "AssignmentStatement" [("identifier", toString n), ("e", toYaml e)]
  toYaml (VariableDeclarationStatement n e) = toMap' "VariableDeclarationStatement" [("identifier", toString n), ("e", toYaml e)]
  toYaml (ConstantDeclarationStatement n e) = toMap' "ConstantDeclarationStatement" [("identifier", toString n), ("e", toYaml e)]
  toYaml (IfThenElseStatement e s1 Nothing) = toMap' "IfThenElseStatement" [("e", toYaml e), ("s1", toYaml s1)]
  toYaml (IfThenElseStatement e s1 (Just s2)) = toMap' "IfThenElseStatement" [("e", toYaml e), ("s1", toYaml s1), ("s2", toYaml s2)]
  toYaml (WhileStatement e s) = toMap' "WhileStatement" [("e", toYaml e), ("s", toYaml s)]
  toYaml (BlockStatement s) = toMap [("BlockStatement", toArray' s)]
  toYaml (CallStatement n e) = toMap' "CallStatement" [("identifier", toString n), ("parameters", toArray' e)]
  toYaml EmptyStatement = toMap' "EmptyStatement" []

instance Yamlable Expression where
  toYaml e@(TernaryExpression e1 e2 e3) = toMap' "TernaryExpression" [("type", toYaml $ Type.typeOf e), ("e1", toYaml e1), ("e2", toYaml e2), ("e3", toYaml e3)]
  toYaml e@(BinaryExpression op e1 e2) = toMap' "BinaryExpression" [("type", toYaml $ Type.typeOf e), ("op", toYaml op), ("e1", toYaml e1), ("e2", toYaml e2)]
  toYaml e@(UnaryExpression op e') = toMap' "UnaryExpression" [("type", toYaml $ Type.typeOf e), ("op", toYaml op), ("e", toYaml e')]
  toYaml (CallExpression t n ps) = toMap' "CallExpression" [("type", toYaml t), ("name", toString n), ("parameters", toArray' ps)]
  toYaml (IdentifierReference t id) = toMap' "IdentifierReference" [("type", toYaml t), ("name", toString id)]
  toYaml (LiteralValue v) = toMap [("LiteralValue", toYaml v)]

instance Yamlable BinaryOp where
  toYaml Divide       = toString "Divide"
  toYaml Minus        = toString "Minus"
  toYaml Plus         = toString "Plus"
  toYaml Times        = toString "Times"
  toYaml Equal        = toString "Equal"
  toYaml GreaterEqual = toString "GreaterEqual"
  toYaml GreaterThan  = toString "GreaterThan"
  toYaml LessEqual    = toString "LessEqual"
  toYaml LessThan     = toString "LessThan"
  toYaml NotEqual     = toString "NotEqual"
  toYaml And          = toString "And"
  toYaml Or           = toString "Or"

instance Yamlable UnaryOp where
  toYaml UnaryNot   = toString "UnaryNot"
  toYaml UnaryMinus = toString "UnaryMinus"
  toYaml UnaryPlus  = toString "UnaryPlus"

instance Yamlable LiteralValue where
  toYaml (LiteralString v) = toMap [("LiteralString", toString $ SC.stringToText $ show v)]
  toYaml (LiteralBool v) = toMap [("LiteralBool", toString $ SC.stringToText $ show v)]
  toYaml (LiteralInt v) = toMap [("LiteralInt", toString $ SC.stringToText $ show v)]
  toYaml (LiteralFloat v) = toMap [("LiteralFloat", toString $ SC.stringToText $ show v)]
