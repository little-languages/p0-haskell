#!/usr/bin/env stack
-- stack --resolver lts-14.18 script

{-# LANGUAGE OverloadedStrings #-}

import Turtle

files = ["hello.c"]

main = do
  shell "clang -emit-llvm -c -S hello.c -o hello.ll" empty
  shell "llvm-as hello.ll -o hello.o" empty
  shell "llvm-link hello.o -o hello.bc" empty
  shell "lli hello.bc" empty
