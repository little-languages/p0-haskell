import qualified Data.ByteString.UTF8 as BLU
import qualified Data.HashMap.Strict  as Map
import qualified Data.Text            as Text
import qualified Data.Vector          as Vector
import qualified Data.Yaml            as Yaml

import           Test.Hspec

import qualified P0.Compiler          as Compiler
import qualified P0.Dynamic           as Dynamic
import           P0.Lexical           (scanner)
import           P0.Static
import qualified P0.StringConversion  as SC
import qualified Toplevel             as TL

import           P0.PrettyPrint

data Tests
  = Describe Text.Text [Tests]
  | Test Text.Text Text.Text Yaml.Value

loadFile :: String -> IO Text.Text
loadFile name = do
  content <- readFile name
  return $ SC.stringToText content

main :: IO ()
main = do
  TL.compileLib'
  lexicalContent <- loadFile "./lexical.yaml"
  staticContent <- loadFile "./parser.yaml"
  dynamicContent <- loadFile "./dynamic.yaml"
  semanticsContent <- loadFile "./semantics.yaml"
  hspec $
    describe "P0" $
    describe "Conformance Tests" $ do
      doTest lexicalAssert $ loadTests "Lexer" lexicalContent
      doTest parserAssert $ loadTests "Static Syntax" staticContent
      doTest dynamicAssert $ loadTests "Dynamic Syntax" dynamicContent
      doTest semanticsAssert $ loadTests "Semantics" semanticsContent
  where
    lexicalAssert input output =
      case output of
        Yaml.Array output' -> input' `shouldBe` vectorToTexts output'
        _                  -> input' `shouldBe` []
      where
        input' = map SC.stringToText $ either ((: []) . show) (map show) $ scanner input
    parserAssert :: Text.Text -> Yaml.Value -> IO ()
    parserAssert input output = (pp . either toYaml toYaml . parse) input `shouldBe` pp output
    dynamicAssert :: Text.Text -> Yaml.Value -> IO ()
    dynamicAssert input output = (pp . either toArray' toYaml . Dynamic.parse) input `shouldBe` pp output
    semanticsAssert :: Text.Text -> Yaml.Value -> IO ()
    semanticsAssert input output = do
      o <- cr input
      o `shouldBe` valueToText output
      where
        cr :: Text.Text -> IO Text.Text
        cr content = either (pure . SC.stringToText . show) TL.run' $ Compiler.parse content
    doTest assert test =
      let doTests' [] = return ()
          doTests' (x:xs) = do
            doTest' x
            doTests' xs
          doTest' (Describe name tests) = describe (SC.textToString name) $ doTests' tests
          doTest' (Test name input output) = it (SC.textToString name) $ assert input output
       in doTest' test

loadTests :: Text.Text -> Text.Text -> Tests
loadTests name content =
  case Yaml.decodeEither' $ BLU.fromString $ SC.textToString content :: Either Yaml.ParseException Yaml.Value of
    Left error -> Describe name [Test "Load file" (SC.stringToText $ show error) (toString "")]
    Right (Yaml.Array a) -> Describe name (loadTestVector a)
    Right _ -> Describe name []

loadTestVector :: Vector.Vector Yaml.Value -> [Tests]
loadTestVector v =
  if Vector.null v
    then []
    else let hd = Vector.head v
             rest = Vector.tail v
             o =
               case hd of
                 Yaml.Object o' -> o'
                 _              -> Map.empty
          in if Map.member "scenario" o
               then case Map.lookupDefault Yaml.Null "scenario" o of
                      Yaml.Object m ->
                        let name = Map.lookupDefault Yaml.Null "name" m
                            tests = Map.lookupDefault Yaml.Null "tests" m
                            testResults =
                              case tests of
                                Yaml.Array tests' -> loadTestVector tests'
                                _                 -> []
                         in Describe (valueToText name) testResults : loadTestVector rest
                      _ -> loadTestVector rest
               else let name = valueToText $ Map.lookupDefault Yaml.Null "name" o
                        input = valueToText $ Map.lookupDefault Yaml.Null "input" o
                        output = Map.lookupDefault Yaml.Null "output" o
                     in Test name input output : loadTestVector rest

vectorToTexts :: Vector.Vector Yaml.Value -> [Text.Text]
vectorToTexts v = Vector.toList $ Vector.map (Text.strip . valueToText) v
