{-# LANGUAGE MultiParamTypeClasses #-}

module Toplevel where

import           Control.Exception       (bracket)
import           Data.String.Conversions
import           Data.Text               (Text)
import qualified Data.Text.IO            as T
import           System.Directory
import           System.IO
import           System.Posix.Temp
import           System.Process

import           LLVM.AST
import           LLVM.Pretty

compile :: Module -> FilePath -> IO ()
compile llvmModule outfile =
  bracket (mkdtemp "build") removePathForcibly $ \buildDir ->
    withCurrentDirectory buildDir $
      -- create temporary file for "output.ll"
     do
      (llvm, llvmHandle) <- mkstemps "output" ".ll"
      let runtime = "./../test/p0lib.c"
      -- write the llvmModule to a file
      T.hPutStrLn llvmHandle (cs $ ppllvm llvmModule)
      hClose llvmHandle
      -- link the runtime with the assembly
      callProcess "clang" ["-Wno-override-module", "-lm", llvm, runtime, "-o", "../" <> outfile]

run :: Module -> IO Text
run llvmModule = do
  compile llvmModule "./a.out"
  result <- cs <$> readProcess "./a.out" [] []
  removePathForcibly "./a.out"
  return result

run' :: Module -> IO Text
run' llvmModule = do
  writeFile "test.ll" $ cs $ ppllvm llvmModule
  callProcess "llvm-as" ["test.ll", "-o", "test.o"]
  callProcess "llvm-link" ["test.o", "./test/p0lib.o", "-o", "test.bc"]
  cs <$> readProcess "lli" ["test.bc"] []

compileLib' :: IO ()
compileLib' = callProcess "clang" ["-emit-llvm", "-c", "-S", "./test/p0lib.c", "-o", "./test/p0lib.o"]
