module Main where

import           Control.Monad.Cont  (forM_)
import           Data.List
import qualified Data.Text           as Text
import qualified LLVM.AST
import           LLVM.Pretty         (ppllvm)
import           System.Directory    (doesFileExist, getModificationTime)
import           System.Environment  (getArgs, getExecutablePath)
import           System.IO
import           System.Process      (callProcess)

import qualified P0.Compiler         as Compiler
import qualified P0.Dynamic          as Dynamic
import qualified P0.Dynamic.TST      as TST
import           P0.Errors
import           P0.Lexical          (Token (..), scanner)
import           P0.PrettyPrint
import           P0.Static
import qualified P0.Static.AST       as AST
import qualified P0.StringConversion as SC

printError :: Error -> IO ()
printError e = do
  print e
  hFlush stdout
  ioError $ userError "Compilation Error"

printErrors :: [Error] -> IO ()
printErrors es = do
  forM_ es print
  hFlush stdout
  ioError $ userError "Compilation Error"

writeToFile :: Text.Text -> String -> IO ()
writeToFile = writeFile . SC.textToString

printTokens :: Text.Text -> [Token] -> IO ()
printTokens output = writeToFile output . show

printAST :: Text.Text -> AST.Program -> IO ()
printAST output = writeToFile output . SC.bsToString . pp . toYaml

printTST :: Text.Text -> TST.Program -> IO ()
printTST output = writeToFile output . SC.bsToString . pp . toYaml

printIR :: Text.Text -> LLVM.AST.Module -> IO ()
printIR output = writeToFile output . SC.dtlToString . ppllvm

lexicalPhase :: Text.Text -> IO ()
lexicalPhase n = do
  (content, baseN) <- namedContent n
  either printError (printTokens $ baseN <> ".lexical.yaml") $ scanner content

astPhase :: Text.Text -> IO ()
astPhase n = do
  (content, baseN) <- namedContent n
  either printError (printAST $ baseN <> ".ast.yaml") $ parse content

tstPhase :: Text.Text -> IO ()
tstPhase n = do
  (content, baseN) <- namedContent n
  either printErrors (printTST $ baseN <> ".tst.yaml") $ Dynamic.parse content

compilePhase :: Text.Text -> IO ()
compilePhase n = do
  (content, baseN) <- namedContent n
  either printErrors (printIR $ baseN <> ".ll") $ Compiler.parse content

names :: Text.Text -> (Text.Text, Text.Text)
names n =
  let n' =
        if ".p0" `Text.isSuffixOf` n
          then n
          else n <> ".p0"
      baseN = Text.take (Text.length n' - 3) n'
   in (n', baseN)

namedContent :: Text.Text -> IO (Text.Text, Text.Text)
namedContent n = do
  let (n', baseN) = names n
  content <- readFile $ SC.textToString n'
  return (SC.stringToText content, baseN)

buildRunBinary :: Text.Text -> Text.Text -> IO ()
buildRunBinary name target = do
  ep <- getExecutablePath
  let libName = dropWhileEnd (/= '/') ep <> "../lib/p0lib.ll"
  let target' = SC.textToString target
  compilePhase name
  callProcess "clang" ["-Wno-override-module", "-lm", target' <> ".ll", libName, "-o", target']
  runBinary target

runBinary :: Text.Text -> IO ()
runBinary name = callProcess (SC.textToString name') []
  where
    name'
      | "/" `Text.isPrefixOf` name = name
      | "." `Text.isPrefixOf` name = name
      | otherwise = "./" `Text.append` name

run :: Text.Text -> IO ()
run n = do
  let (n', baseN) = names n
  sourceExists <- doesFileExist $ SC.textToString n'
  binaryExists <- doesFileExist $ SC.textToString baseN
  if not sourceExists
    then showError $ "Source file does not exist: " <> SC.textToString n'
    else if binaryExists
           then do
             sourceDateTime <- getModificationTime $ SC.textToString n'
             binaryDateTime <- getModificationTime $ SC.textToString baseN
             if sourceDateTime > binaryDateTime
               then buildRunBinary n' baseN
               else runBinary baseN
           else buildRunBinary n' baseN

showError :: String -> IO ()
showError error = putStrLn $ "Error: " <> error

showOptions :: String -> IO ()
showOptions error = do
  showError error
  putStrLn "Options: --help | run file | compile file [--phase (lexical | ast | tst | compile)]"
  putStrLn "  --help"
  putStrLn "    Show all help options."
  putStrLn ""
  putStrLn "  run file"
  putStrLn "    Run the passed file.  If the content file[.p0] is more recent that file then the P0"
  putStrLn "    is first compiled before the binary version is executed."
  putStrLn ""
  putStrLn "  compile file"
  putStrLn "    Compiles the passed file to a the associated phase content.  If no phase is supplied"
  putStrLn "    then the phase defaults to compile.  Note that this option is a force compile."
  putStrLn "  --phase (lexical | ast | tst | compile | all)"
  putStrLn "      lexical: produces YAML content of the tokenized source file."
  putStrLn "      ast: produces YAML content of the abstract syntax tree."
  putStrLn "      tst: produces YAML content of the typed abstract syntax tree."
  putStrLn "      compile: produces compiled IR text of the program."
  putStrLn "      all: produces all of the intermediary results."

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> showOptions "No arguments supplied"
    ["run", n] -> run $ SC.stringToText n
    ["compile", n] -> compilePhase $ SC.stringToText n
    ["compile", n, "--phase", p] ->
      case p of
        "lexical" -> lexicalPhase $ SC.stringToText n
        "ast" -> astPhase $ SC.stringToText n
        "tst" -> tstPhase $ SC.stringToText n
        "compile" -> compilePhase $ SC.stringToText n
        "all" -> do
          lexicalPhase $ SC.stringToText n
          astPhase $ SC.stringToText n
          tstPhase $ SC.stringToText n
          compilePhase $ SC.stringToText n
        _ -> showOptions $ "Illegal phase name: " <> n
    ["--help"] -> print args
    _ -> showOptions ("Invalid arguments: " <> unwords args)
